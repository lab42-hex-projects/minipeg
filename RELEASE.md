# v0.6.0 2024-03-09

- `map_error` and `set_error` combinators

# v0.5.4 2024-02-29

- Debug formatter, Issue #3

- Bad position update in ignore_ws parser, Issue #1

# v0.5.3 2024-02-23

- Success and Failure get a `parsed_by` field

# v0.5.2 2024-02-22

- debug can take a function now

# v0.5.1 2024-02-21

- `with_pos` as shortcut for `mapp(..., &{&1, &2}, ...)`

# v0.5.0 2024-02-21

- Add parsing position to `%Sucess{}` for runtime analysis

- Add mapp to get also the postion where the ast was parsed from

# v0.4.2 2024-02-19

- many_seq combinator [✓]
- many_sel combinator [✓]
_ option alias for select [✓]

# v0.4.1 2024-02-18

- Better error messages in `parse_string`

# v0.2.8

- Improving documentation
    - Doctests as a manual
    - More function docs for parsers and combinators
