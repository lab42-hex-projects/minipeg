defmodule Support.ParserTestCase do
  @moduledoc ~S"""
  Common aliases and imports for testing parsers
  """

  defmacro __using__(_opts) do
    quote do
      use ExUnit.Case

      alias Minipeg.{Cache, Combinators, Failure, Formatter, Input, Mappers, Parser, Parsers, Success}
      import Combinators
      import Mappers
      import Parsers

      defp assert_failure(parser, input, expected_reason, col \\ 1, lnb \\ 1) do
        input1 = make_input(input, col, lnb)
        %Failure{} = result = parse(parser, input1)
        assert Failure.error_message(result) == expected_reason
        assert result.input == input1
      end

      defp assert_parse_string_ok(parser, input, result) do
        with {:ok, ast} <- Parser.parse_string(parser, input) do
          assert ast == result
        end
      end

      defp assert_success(parser, input, expected_ast, expected_rest, col \\ 1, lnb \\ 1)

      defp assert_success(parser, input, expected_ast, expected_rest, col, lnb)
           when is_binary(expected_rest) do
        assert_success(parser, input, expected_ast, make_input(expected_rest), col, lnb)
      end

      defp assert_success(parser, input, expected_ast, {r, c}, col, lnb) do
        assert_success(parser, input, expected_ast, make_input(r, c), col, lnb)
      end

      defp assert_success(parser, input, expected_ast, expected_rest, col, lnb) do
        %Success{} = result = parse(parser, input, col, lnb)
        assert result.ast == expected_ast
        assert result.rest == expected_rest
        result
      end

      defp make_input(str, col \\ 1, lnb \\ 1)

      defp make_input(str, col, lnb) when is_binary(col),
        do: %Input{input: str, col: 1 + String.length(col), lnb: lnb}

      defp make_input(str, col, lnb) when is_number(col),
        do: %Input{input: str, col: col, lnb: lnb}

      defp parse(parser, input, col \\ 1, lnb \\ 1)

      defp parse(parser, input, col, lnb) when is_binary(input) do
        Parser.parse(parser, make_input(input, col, lnb), %Cache{}) |> _insp(parser.name)
      end

      defp parse(parser, %Input{} = input, _col, _lnb) do
        Parser.parse(parser, input, %Cache{}) |> _insp(parser.name)
      end

      defp _insp(value, name \\ nil) do
        if System.get_env("DEBUG") do
          # IO.inspect({value, name})
        end

        value
      end
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
