defmodule Support.SimpleTagParser do
  alias Minipeg.{Combinators, Parsers}
  import Combinators
  import Parsers

  @moduledoc ~S"""
  Parse a rudimentary tag language which tests recursiveness with lazy

    tag ::= 
       "<div>" tag "</div>"
      | text
    
    text ::=  !("<")*
    
  """
  def tag_parser do
    select([
      sequence([
        keywords_parser(["<div>"]),
        lazy(fn -> tag_parser() end),
        keywords_parser(["</div>"])
      ]),
      text_parser()
    ])
  end

  def text_parser do
    many(not_char_parser("<")) |> map(&Enum.join/1)
  end
end

# SPDX-License-Identifier: Apache-2.0
