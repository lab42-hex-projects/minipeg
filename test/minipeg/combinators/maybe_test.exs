defmodule Test.Minipeg.Combinators.MaybeTest do
  use Support.ParserTestCase

  describe "maybe" do
    test "parses a" do
      assert_success(subject(), "a", "a", make_input("", 2))
    end

    test "parses empty" do
      assert_success(subject(), "", nil, "")
    end

    test "parses b" do
      assert_success(subject(), "b", nil, "b")
    end
  end

  defp subject, do: maybe(char_parser("a"))
end

# SPDX-License-Identifier: Apache-2.0
