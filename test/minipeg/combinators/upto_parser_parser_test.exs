defmodule Test.Minipeg.Combinators.UptoParserParserTestCase do
  use Support.ParserTestCase

  describe "utpo_parser_parser" do
    test "empty fails" do
      assert_failure(
        upto_a_parser(),
        "",
        "encountered end of input (in upto_parser)"
      )
    end

    test "empty fails with :discard" do
      assert_failure(
        upto_a_parser(:discard),
        "",
        "encountered end of input (in upto_parser)"
      )
    end

    test "empty fails with :include" do
      assert_failure(
        upto_a_parser(:include),
        "",
        "encountered end of input (in upto_parser)"
      )
    end

    test "never found fails" do
      assert_failure(
        upto_a_parser(),
        "",
        "encountered end of input (in upto_parser)",
        4
      )
    end

    test "empty success" do
      assert_success(
        upto_a_parser(),
        "a",
        "",
        "a"
      )
    end

    test "not empty success" do
      assert_success(
        upto_a_parser(),
        "beta",
        "bet",
        make_input("a", 4)
      )
    end
  end

  defp upto_a_parser(mode \\ :keep), do: upto_parser_parser(char_parser("a"), "upto_parser", mode)
end

# SPDX-License-Identifier: Apache-2.0
