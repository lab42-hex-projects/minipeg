defmodule Test.Minipeg.Combinators.LazyParserTestCase do
  use Support.ParserTestCase

  import Support.SimpleTagParser

  describe "simple_tag_parser" do
    test "parses text" do
      assert_success(
        text_parser(),
        "ab>",
        "ab>",
        make_input("", 4)
      )
    end

    test "parses one div" do
      assert_success(
        tag_parser(),
        "<div>hello</div>",
        ["<div>", "hello", "</div>"],
        make_input("", 17)
      )
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
