defmodule Test.Minipeg.Combinators.MapToStringTest do
  use Support.ParserTestCase

  describe "ignore rem string success" do
    test "empty" do
      assert_success(subject(), "", "", make_input(""))
    end

    test "not empty" do
      assert_success(subject(), "The quick", "The quick", make_input("", "The quick"))
    end
  end

  defp subject do
    many(char_parser())
    |> map_to_string
  end
end

# SPDX-License-Identifier: Apache-2.0
