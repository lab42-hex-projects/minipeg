defmodule Test.Minipeg.Combinators.DebugParserTestCase do
  use Support.ParserTestCase
  import ExUnit.CaptureIO

  describe "debug parser" do
    test "around the failure_parser" do
      output =
        capture_io(:stderr, fn ->
          assert_failure(debug(failure_parser("reason", "name")), "", "reason (in name)")
        end)

      assert output ==
               """
               %{input: %Minipeg.Input{col: 1, context: %{}, input: "", lnb: 1}, name: \"name\", cache: %Minipeg.Cache{cache: %{}}}\n%Minipeg.Failure{cache: %Minipeg.Cache{cache: %{}}, input: %Minipeg.Input{col: 1, context: %{}, input: "", lnb: 1}, parsed_by: \"name\", reason: \"reason\"}
               """
    end

    test "around the success_parser" do
      output =
        capture_io(:stderr, fn ->
          assert_success(debug(success_parser(42, "always right")), "", 42, "")
        end)

      assert output ==
               """
               %{input: %Minipeg.Input{col: 1, context: %{}, input: "", lnb: 1}, name: \"always right\", cache: %Minipeg.Cache{cache: %{}}}\n%Minipeg.Success{ast: 42, cache: %Minipeg.Cache{cache: %{}}, parsed_at: {1, 1}, parsed_by: "always right", rest: %Minipeg.Input{col: 1, context: %{}, input: "", lnb: 1}}
               """
    end

    test "with a function" do
      custom_fun = fn %Input{col: col}, %Success{parsed_at: position} -> IO.puts(inspect {col, position}) end
      output =
        capture_io(fn ->
          # assert_success(debug(success_parser(42, "always right"), nil, custom_fun), "", 42, "")
          parse(debug(success_parser(42, "always right"), nil, custom_fun), %Input{})
        end)

      assert output ==
               """
               {1, {1, 1}}
               """
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
