defmodule Test.Minipeg.Combinators.ListParserTestCase do
  use Support.ParserTestCase

  describe "list_parser" do
    test "successful testing" do
      parser =
        list_parser(
          many(not_char_parser(","), nil, 1) |> map(&Enum.join/1),
          sequence([char_parser(","), ws_parser()])
        )

      input = "ax, b,c"

      assert_success(
        parser,
        input,
        ["ax", "b", "c"],
        make_input("", input)
      )
    end

    test "unsuccessful testing" do
      parser =
        list_parser(
          many(not_char_parser(","), nil, 1) |> map(&Enum.join/1),
          sequence([char_parser(","), ws_parser()]),
          nil,
          1
        )

      input = ""

      assert_failure(
        parser,
        input,
        "Parser list_parser(not_char_parser(,), sequence) only parsed 0 element(s) but 1 were needed (in maybe(sequence))"
      )
    end

    test "unsuccessful testing one element" do
      parser =
        list_parser(
          many(not_char_parser(","), nil, 1) |> map(&Enum.join/1),
          sequence([char_parser(","), ws_parser()]),
          "double parser",
          2
        )

      input = "a"

      assert_failure(
        parser,
        input,
        "Parser double parser only parsed 1 element(s) but 2 were needed (in maybe(sequence))"
      )
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
