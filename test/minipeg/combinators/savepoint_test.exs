defmodule Test.Minipeg.Combinators.SavepointTest do
  use Support.ParserTestCase
  import ExUnit.CaptureIO

  describe "savepoint" do
    test "succeeds and only one call to map" do
      output =
        capture_io(:stdio, fn ->
          %Success{ast: ast, rest: rest, cache: cache} =
            parse(subject(), make_input("a"), %Cache{})

          assert ast == "a"
          assert rest == make_input("", 2)

          assert cache == %Cache{
                   cache: %{
                     {1, 1} => %{
                       "sp1" => {:success, "a", %Minipeg.Input{input: "", col: 2, lnb: 1}, {1, 1}}
                     }
                   }
                 }
        end)

      assert output == "#{delimiter()}\n"
    end
  end

  defp subject do
    select([
      sequence([
        savepoint(parser1(), "sp1"),
        failure_parser()
      ]),
      savepoint(parser1(), "sp1")
    ])
  end

  defp delimiter(), do: "************"

  defp parser1 do
    char_parser()
    |> map(fn ast ->
      IO.puts(delimiter())
      ast
    end)
  end
end

# SPDX-License-Identifier: Apache-2.0
