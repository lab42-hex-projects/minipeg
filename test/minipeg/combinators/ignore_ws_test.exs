defmodule Test.Minipeg.Combinators.IgnoreWsTest do
  use Support.ParserTestCase

  describe "ignore_ws success" do
    test "empty" do
      assert_success(ignore_ws(end_parser()), "", nil, make_input(""))
    end

    test "only ws" do
      success = assert_success(ignore_ws(end_parser()), " \t", nil, make_input("", 3))
      assert success.parsed_at == {3, 1}
    end

    test "only ws custom position" do
      success = assert_success(ignore_ws(end_parser()), " \t", nil, make_input("", 3))
      assert success.parsed_at == {3, 1}
    end

    test "with newlines" do
      success = assert_success(
        ignore_ws(end_parser(), "my parser", true),
        " \t\n",
        nil,
        make_input("", 1, 2)
      )
      assert success.parsed_at == {1, 2}
    end

    test "still getting result" do
      success = assert_success(
        ignore_ws(many(char_parser("x")), "my parser", true),
        " \t\nxxx",
        String.graphemes("xxx"),
        make_input("", 4, 2)
      )
      # IO.inspect(success)
      assert success.parsed_at == {1, 2}
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
