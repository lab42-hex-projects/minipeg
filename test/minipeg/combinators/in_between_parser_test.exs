defmodule Test.Minipeg.Combinators.InBetweenParserTestCase do
  use Support.ParserTestCase

  describe "in_between_parser" do
    test "successful testing" do
      input = ":1,2,3:4"

      assert_success(
        subject(),
        input,
        [1, 2, 3],
        make_input("4", 8)
      )
    end

    test "unsuccessful; missing end" do
      input = ":1,2"

      assert_failure(
        subject(),
        input,
        "encountered end of input (in char_parser(\":\"))"
      )
    end

    test "unsuccessful; missing start" do
      input = "1,2:"

      assert_failure(
        subject(),
        input,
        "1 not member of \":\" in char_parser(\":\") (in char_parser(\":\"))"
      )
    end

    test "unnamed" do
      parser =
        in_between_parser(
          char_parser(":"),
          list_parser(tokenize(int_parser()), char_parser(",")),
          char_parser(":")
        )

      input = ":  1, 2 :"

      assert_success(
        parser,
        input,
        [1, 2],
        make_input("", input)
      )
    end
  end

  defp subject do
    in_between_parser(
      char_parser(":"),
      list_parser(ignore_ws(int_parser()), char_parser(",")),
      char_parser(":"),
      "test01"
    )
  end
end

# SPDX-License-Identifier: Apache-2.0
