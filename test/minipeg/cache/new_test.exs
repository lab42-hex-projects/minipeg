defmodule Test.Minipeg.Cache.NewTest do
  use ExUnit.Case

  alias Minipeg.{Cache, Failure, Input, Success}

  import Cache, only: [new: 3]
  import Failure, only: [fail: 3]
  import Success, only: [succeed: 4]

  describe "Cache" do
    test "factory for success" do
      input = %Input{col: 10, lnb: 2, input: ""}
      cache = new({2, 3}, "some parser", succeed("some result", input, nil, {100, 200}))

      assert cache == %Cache{
               cache: %{
                 {2, 3} => %{"some parser" => {:success, "some result", input, {100, 200}}}
               }
             }
    end

    test "factory for failurs" do
      input = %Input{col: 10, lnb: 2, input: ""}
      cache = new({2, 3}, "some parser", fail("some error", input, nil))

      assert cache == %Cache{
               cache: %{{2, 3} => %{"some parser" => {:failure, "some error", input}}}
             }
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
