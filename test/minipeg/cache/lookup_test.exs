defmodule Test.Minipeg.Cache.LookupTest do
  use ExUnit.Case

  alias Minipeg.{Cache, Failure, Input, Success}

  import Cache, only: [lookup: 3]

  describe "no hits" do
    test "no hit" do
      assert lookup(cache(), no_input(), "x") == nil
    end

    test "still no hit" do
      assert lookup(cache(), failure_input(), "bad name") == nil
    end
  end

  describe "hits" do
    test "failure" do
      assert lookup(cache(), failure_input(), "b") ==
               %Failure{reason: "reason", input: input2(), cache: cache()}
    end

    test "success" do
      assert lookup(cache(), success_input(), "a") ==
               %Success{ast: "ast", rest: input1(), cache: cache(), parsed_at: {3, 7}}
    end
  end

  defp cache,
    do: %Cache{
      cache: %{
        {2, 2} => %{"a" => {:success, "ast", input1(), {3, 7}}},
        {2, 3} => %{"b" => {:failure, "reason", input2()}}
      }
    }

  defp no_input, do: %Input{col: 3, lnb: 3}
  defp failure_input, do: %Input{col: 2, lnb: 3}
  defp success_input, do: %Input{col: 2, lnb: 2}
  defp input1, do: %Input{col: 10, lnb: 2, input: ""}
  defp input2, do: %Input{col: 1, lnb: 12, input: "ab"}
end

# SPDX-License-Identifier: Apache-2.0
