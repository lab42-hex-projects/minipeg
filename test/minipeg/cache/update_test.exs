defmodule Test.Minipeg.Cache.UpdateTest do
  use ExUnit.Case

  alias Minipeg.{Cache, Failure, Input, Success}

  import Cache, only: [update: 4]
  import Failure, only: [fail: 3]
  import Success, only: [succeed: 4]

  describe "update like new" do
    test "with success" do
      cache = update(%Cache{}, {1, 4}, "key", succeed("ast", input(), nil, {1, 1}))
      assert cache == %Cache{cache: %{{1, 4} => %{"key" => {:success, "ast", input(), {1, 1}}}}}
    end

    test "with failure" do
      cache = update(%Cache{}, {1, 4}, "key", fail("reason", input(), nil))
      assert cache == %Cache{cache: %{{1, 4} => %{"key" => {:failure, "reason", input()}}}}
    end
  end

  describe "update existing" do
    test "update no values" do
      cache = update(old(), {2, 3}, "some parser", succeed("some result", input(), nil, {1, 1}))

      assert cache ==
               %Cache{
                 cache: %{
                   {2, 2} => %{"a" => {:success, "ast", %Input{}}},
                   {2, 3} => %{"some parser" => {:success, "some result", input(), {1, 1}}}
                 }
               }
    end

    test "update same values" do
      cache = update(with_value(), {2, 3}, "some parser", fail("some error", input(), nil))

      assert cache ==
               %Cache{
                 cache: %{
                   {2, 2} => %{"a" => {:success, "ast", %Input{}}},
                   {2, 3} => %{
                     "some parser" => {:failure, "some error", input()},
                     "other" => :some_value
                   }
                 }
               }
    end

    defp old, do: %Cache{cache: %{{2, 2} => %{"a" => {:success, "ast", %Input{}}}}}

    defp with_value,
      do: %Cache{
        cache: %{
          {2, 2} => %{"a" => {:success, "ast", %Input{}}},
          {2, 3} => %{"some parser" => :old_value, "other" => :some_value}
        }
      }
  end

  defp input, do: %Input{col: 10, lnb: 2, input: ""}
end

# SPDX-License-Identifier: Apache-2.0
