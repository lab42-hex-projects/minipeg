defmodule Test.Minipeg.Parsers.PrefixedParserTestCase do
  use Support.ParserTestCase

  describe "prefixed_parser succeeding" do
    test "on empty (suffix)" do
      assert_success(
        prefixed_parser(["hello"], end_parser()),
        "hello",
        nil,
        make_input("", 6)
      )
    end

    test "with parsing something" do
      assert_success(
        prefixed_parser(["hello"], int_parser()),
        "hello42",
        42,
        make_input("", 8)
      )
    end

    test "matching an alternative" do
      assert_success(
        prefixed_parser(["world", "hello"], int_parser()),
        "hello42",
        42,
        make_input("", 8)
      )
    end

    test "matching a letter" do
      assert_success(
        prefixed_parser("01", int_parser()),
        "142",
        42,
        make_input("", 4)
      )
    end
  end

  describe "prefixed_parser failing" do
    test "prefix does not match" do
      assert_failure(
        prefixed_parser(["hello"], end_parser()),
        "hellx",
        "none of the strings [\"hello\"] found at current position (in keywords_parser([\"hello\"]))"
      )
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
