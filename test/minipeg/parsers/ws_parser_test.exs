defmodule Test.Minipeg.Parsers.WsParserTestCase do
  use Support.ParserTestCase

  describe "ws_parser min_count == 0" do
    test "succeds on empty" do
      assert_success(
        ws_parser(),
        "",
        [],
        ""
      )
    end

    test "succeds on empty (newline)" do
      assert_success(
        ws_parser(true),
        "",
        [],
        ""
      )
    end

    test "succeeds and returns tabs" do
      assert_success(ws_parser(), "\t", ["\t"], make_input("", 2))
    end

    test "succeeds and returns tabs and spaces (newline)" do
      assert_success(ws_parser(true), "\t ", ["\t", " "], make_input("", 3))
    end

    test "succeeds but does not parse newlines" do
      assert_success(ws_parser(), " \nrest", [" "], make_input("\nrest", 2))
    end

    test "succeeds and parses newlines" do
      assert_success(ws_parser(true), " \nrest", [" ", "\n"], make_input("rest", 1, 2))
    end
  end

  describe "ws_parser min_count > 0" do
    test "fails on empty" do
      assert_failure(
        ws_parser(false, 1),
        "",
        "Missing 1 parses in many (in ws_parser(false, 1))"
      )
    end

    test "fails on empty (newline)" do
      assert_failure(
        ws_parser(true, 1),
        "",
        "Missing 1 parses in many (in ws_parser(true, 1))"
      )
    end

    test "fails on newline" do
      assert_failure(
        ws_parser(false, 1),
        "\n",
        "Missing 1 parses in many (in ws_parser(false, 1))"
      )
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
