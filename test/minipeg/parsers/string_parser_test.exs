defmodule Test.Minipeg.Parsers.StringParserTestCase do
  use Support.ParserTestCase

  describe "not strings" do
    test "empty input" do
      assert_failure(
        string_parser(),
        "",
        "no alternative could be parsed in string_parser (in string_parser)"
      )
    end

    test "not starting with a quote" do
      assert_failure(
        string_parser(),
        "a'",
        "no alternative could be parsed in string_parser (in string_parser)"
      )
    end

    test "missing closing double quote" do
      assert_failure(
        string_parser(),
        ~s{"a'},
        "no alternative could be parsed in string_parser (in string_parser)"
      )
    end

    test "missing closing single quote" do
      assert_failure(
        string_parser(),
        ~s{'"a},
        "no alternative could be parsed in string_parser (in string_parser)"
      )
    end

    test "doubled double quote does not close string" do
      assert_failure(
        string_parser(),
        ~s{"a""b},
        "no alternative could be parsed in string_parser (in string_parser)"
      )
    end

    test "doubled single quote does not close string" do
      assert_failure(
        string_parser(),
        ~s{'"a''},
        "no alternative could be parsed in string_parser (in string_parser)"
      )
    end

    test "escaped double quote does not close string" do
      assert_failure(
        string_parser(),
        ~s{"a\\"b},
        "no alternative could be parsed in string_parser (in string_parser)"
      )
    end

    test "escaped single quote does not close string" do
      assert_failure(
        string_parser(),
        ~s{'"a\\'},
        "no alternative could be parsed in string_parser (in string_parser)"
      )
    end
  end

  describe "double quoted string" do
    test "empty string" do
      assert_success(
        string_parser(),
        ~s{""},
        "",
        {"", 3}
      )
    end

    test "no special chars" do
      assert_success(
        string_parser(),
        ~s{"abc"d"},
        "abc",
        {~s{d"}, 6}
      )
    end

    test "single quotes" do
      assert_success(
        string_parser(),
        ~s{"a'c"d"},
        "a'c",
        {~s{d"}, 6}
      )
    end

    test "escaped double quotes" do
      assert_success(
        string_parser(),
        ~s{"a\\"c"d"},
        ~s{a"c},
        {~s{d"}, 7}
      )
    end

    test "doubled double quotes" do
      assert_success(
        string_parser(),
        ~s{"a""c"d"},
        ~s{a"c},
        {~s{d"}, 7}
      )
    end

    test "doubled double quotes enabled" do
      assert_success(
        string_parser(~W["], "\\"),
        ~s{"a""c"d"},
        ~s{a"c},
        {~s{d"}, 7}
      )
    end

    test "doubled double quotes disabled" do
      assert_success(
        string_parser(~W["], "\\", false),
        ~s{"a""c"d"},
        ~s{a},
        {~s{"c"d"}, 4}
      )
    end

    test "all of the above" do
      assert_success(
        string_parser(),
        ~s{"a""'\\""d"},
        ~s{a"'"},
        {~s{d"}, 9}
      )
    end
  end

  describe "single quoted string" do
    test "empty string" do
      assert_success(
        string_parser(),
        ~s{''},
        "",
        {"", 3}
      )
    end

    test "no special chars" do
      assert_success(
        string_parser(),
        ~s{'abc'd'},
        "abc",
        {~s{d'}, 6}
      )
    end

    test "double quotes" do
      assert_success(
        string_parser(),
        ~s{'a"c'd'},
        ~s{a"c},
        {~s{d'}, 6}
      )
    end

    test "escaped single quotes" do
      assert_success(
        string_parser(),
        ~s{'a\\'c'd'},
        ~s{a'c},
        {~s{d'}, 7}
      )
    end

    test "doubled single quotes" do
      assert_success(
        string_parser(),
        ~s{'a''c'd'},
        ~s{a'c},
        {~s{d'}, 7}
      )
    end

    test "all of the above" do
      assert_success(
        string_parser(),
        ~s{'a''"\\''d'},
        ~s{a'"'},
        {~s{d'}, 9}
      )
    end
  end

  describe "customized" do
    test "forbid double quotes" do
      assert_success(
        string_parser("'", "", false),
        ~s{'a''c'd'},
        ~s{a},
        {~s{'c'd'}, 4}
      )
    end

    test "custom parens" do
      assert_success(
        string_parser("x"),
        ~s{x"xy},
        ~s{"},
        {~s{y}, 4}
      )
    end

    test "all of the above" do
      assert_success(
        string_parser("xy"),
        ~s{yxyy\\yyx},
        ~s{xyy},
        {~s{x}, 8}
      )
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
