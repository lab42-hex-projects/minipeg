defmodule Test.Minipeg.Parsers.KeywordsParserTestCase do
  use Support.ParserTestCase

  describe "keywords_parser failing" do
    test "on empty" do
      assert_failure(
        subject(),
        "",
        "no alternative could be parsed in my parser (in my parser)"
      )
    end

    test "not a match" do
      assert_failure(
        subject(),
        "alphx",
        "no alternative could be parsed in my parser (in my parser)"
      )
    end

    test "no leading 0s" do
      assert_failure(
        subject(),
        "01",
        "no alternative could be parsed in my parser (in my parser)"
      )
    end
  end

  describe "keywords_parser succeeding" do
    test "on first keyword" do
      assert_success(
        subject(),
        "alphax",
        "alpha",
        make_input("x", 6)
      )
    end

    test "on second keyword" do
      assert_success(
        subject(),
        "beta",
        "beta",
        make_input("", 5)
      )
    end

    test "on third keyword" do
      assert_success(
        subject(),
        "gammaalpha",
        "gamma",
        make_input("alpha", 6)
      )
    end
  end

  defp subject, do: keywords_parser(["alpha", "beta", "gamma"], "my parser")
end

# SPDX-License-Identifier: Apache-2.0
