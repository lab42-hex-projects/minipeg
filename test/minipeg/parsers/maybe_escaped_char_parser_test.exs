defmodule Test.Minipeg.Parsers.MaybeEscapedCharParserTestCase do
  use Support.ParserTestCase

  describe "cases not covered by doctests" do
    test "ending" do
      assert_success(
        maybe_escaped_char_parser(~s{"}),
        ~s{x"},
        "x",
        make_input("\"", 2)
      )
    end

    test "not ending escape no doubles" do
      assert_success(
        maybe_escaped_char_parser(~s{"}, ".", false),
        ~s{."},
        "\"",
        make_input("", 3)
      )
    end

    test "not ending doubles" do
      assert_success(
        maybe_escaped_char_parser(~s{"}, "."),
        ~s{""},
        "\"",
        make_input("", 3)
      )
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
