defmodule Test.Minipeg.Parsers.EndParserTestCase do
  use Support.ParserTestCase

  describe "end_parser" do
    test "succeeds on empty" do
      assert_success(end_parser(), "", nil, "")
    end

    test "fails on one char" do
      assert_failure(end_parser(), "H", "not at end of input @ {1, 1} in end_parser (in end_parser)")
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
