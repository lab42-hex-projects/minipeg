defmodule Test.Minipeg.Parsers.IntParserTestCase do
  use Support.ParserTestCase

  describe "int_parser" do
    test "fails with empty input" do
      assert_failure(
        int_parser(),
        "",
        "Missing 1 parses in many (in int_parser)"
      )
    end

    test "fails with a non digit" do
      assert_failure(
        int_parser(),
        "a",
        "Missing 1 parses in many (in int_parser)"
      )
    end

    test "fails with a leading sign" do
      assert_failure(
        int_parser(),
        "*1",
        "Missing 1 parses in many (in int_parser)"
      )

      assert_failure(
        int_parser(),
        "/1",
        "Missing 1 parses in many (in int_parser)"
      )
    end

    test "succeeds with one digit" do
      assert_success(
        int_parser(),
        "0",
        0,
        make_input("", 2)
      )

      assert_success(
        int_parser(),
        "1",
        1,
        make_input("", 2)
      )

      assert_success(
        int_parser(),
        "9a",
        9,
        make_input("a", 2)
      )
    end

    test "succeeds with more digits" do
      assert_success(
        int_parser(),
        "40a",
        40,
        make_input("a", 3)
      )
    end

    test "succeeds with a leading + sign" do
      assert_success(
        int_parser(),
        "+40a",
        40,
        make_input("a", 4)
      )
    end

    test "succeeds with a leading - sign" do
      assert_success(
        int_parser(),
        "-42a",
        -42,
        make_input("a", 4)
      )
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
