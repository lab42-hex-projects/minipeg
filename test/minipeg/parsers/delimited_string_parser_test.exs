defmodule Test.Minipeg.Parsers.DelimitedStringParserTestCase do
  use Support.ParserTestCase

  describe "delimited string, no escapes, no repeats" do
    test "empty string" do
      assert_success(
        delimited_string_parser(~s{"}),
        ~s{""},
        [],
        make_input("", 3)
      )
    end

    test "string with some content" do
      assert_success(
        delimited_string_parser(~s{"}),
        ~s{"ab"},
        ["a", "b"],
        make_input("", 5)
      )
    end

    test "string with some content ended" do
      assert_success(
        delimited_string_parser(~s{"}),
        ~s{"ab"c},
        ["a", "b"],
        make_input(~s{c}, 5)
      )
    end

    test "string with different escape char" do
      assert_success(
        delimited_string_parser(~s{"}, "."),
        ~s{"ab.""c},
        ["a", "b", "\""],
        make_input(~s{c}, 7)
      )
    end

    test "string with some content, ended by double" do
      assert_success(
        delimited_string_parser(~s{"}, "x", false),
        ~s{"ab""c},
        ["a", "b"],
        make_input(~s{"c}, 5)
      )
    end

    test "allow doubling" do
      assert_success(
        delimited_string_parser(~s{"}, "x", true),
        ~s{"ab""c"},
        ["a", "b", ~s{"}, "c"],
        {"", 8}
      )
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
