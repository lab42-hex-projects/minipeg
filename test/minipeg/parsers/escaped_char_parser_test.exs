defmodule Test.Minipeg.Parsers.EscapedCharParserTestCase do
  use Support.ParserTestCase

  describe "escaped char parser with defaults" do
    test "succeeds with leading \\" do
      assert_success(
        escaped_char_parser(),
        "\\a\\",
        "a",
        make_input("\\", 3)
      )
    end

    test "needs two characters" do
      assert_failure(
        escaped_char_parser(),
        "\\",
        "encountered end of input (in escaped_char_parser)"
      )
    end

    test "needs leading escape" do
      assert_failure(
        escaped_char_parser(),
        "a",
        "a not member of \"\\\\\" in char_parser(\"\\\\\") (in escaped_char_parser)"
      )
    end
  end

  describe "other escape char" do
    test "success" do
      assert_success(subject(), ",b", "b", make_input("", 3))
    end

    test "failure" do
      assert_failure(
        subject(),
        "\\b",
        "\\ not member of \",\" in char_parser(\",\") (in escaped_char_parser)"
      )
    end

    defp subject, do: escaped_char_parser(",", "comma-escaper")
  end
end

# SPDX-License-Identifier: Apache-2.0
