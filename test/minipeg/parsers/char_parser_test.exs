defmodule Test.Minipeg.Parsers.CharParserTestCase do
  use Support.ParserTestCase

  describe "char_parser, any char" do
    test "fails on empty" do
      assert_failure(char_parser(nil, "char_parser()"), "", "encountered end of input (in char_parser())")
    end

    test "succeeds on one char" do
      assert_success(char_parser(), "H", "H", make_input("", 2))
    end

    test "succeds on longer input" do
      assert_success(char_parser(), "Hello", "H", make_input("ello", 2))
    end
  end

  describe "char_parser only digits" do
    test "fails on empty" do
      assert_failure(
        char_parser("1234567890"),
        "",
        "encountered end of input (in char_parser(\"1234567890\"))"
      )
    end

    test "fails on letter" do
      assert_failure(
        char_parser("1234567890"),
        "Hello",
        "H not member of \"1234567890\" in char_parser(\"1234567890\") (in char_parser(\"1234567890\"))"
      )
    end

    test "succeeds on number" do
      assert_success(char_parser("1234567890"), "42", "4", make_input("2", 2))
    end
  end

  describe "char_parser by list" do
    test "fails on empty" do
      assert_failure(
        char_parser(["a", "b"]),
        "",
        ~s{encountered end of input (in char_parser(["a", "b"]))}
      )
    end

    test "succeeds on a" do
      assert_success(char_parser(~W[a b]), "a", "a", make_input("", 2))
    end

    test "succeeds on bxa" do
      assert_success(char_parser(~W[a b]), "bxa", "b", make_input("xa", 2))
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
