defmodule Test.Minipeg.Parsers.LispyParser.SExpParserTestCase do
  use Support.ParserTestCase

  import Minipeg.Parsers.LispyParser, only: [s_exp_parser: 0]

  describe "failing" do
    test "empty" do
      assert_failure(
        s_exp_parser(),
        "",
        "encountered end of input (in char_parser(\"(\"))"
      )
    end

    test "not an opening \"(\"" do
      assert_failure(
        s_exp_parser(),
        "Hello",
        "H not member of \"(\" in char_parser(\"(\") (in char_parser(\"(\"))"
      )
    end

    test "not a closing \")\"" do
      assert_failure(
        s_exp_parser(),
        "(Hello 42",
        "encountered end of input (in char_parser(\")\"))"
      )
    end
  end

  describe "simple s-expressions" do
    test "empty" do
      assert_success(
        s_exp_parser(),
        "()",
        [],
        make_input("", 3)
      )
    end

    test "list of elements" do
      input = "(alpha 42)"

      assert_success(
        s_exp_parser(),
        input,
        [:alpha, 42],
        make_input("", input)
      )
    end

    test "with strings and rest" do
      input = ~s{(+ "hello" "world")}
      rest = "some"

      assert_success(
        s_exp_parser(),
        input <> rest,
        [:+, "hello", "world"],
        make_input(rest, input)
      )
    end
  end

  describe "more levels" do
    test "that should pretty much cover it ;)" do
      input = "((lambda x (+ x 1)) (+ 39 1 1))"

      assert_success(
        s_exp_parser(),
        input,
        [[:lambda, :x, [:+, :x, 1]], [:+, 39, 1, 1]],
        make_input("", input)
      )
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
