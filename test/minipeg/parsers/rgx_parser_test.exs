defmodule Test.Minipeg.Parsers.RegexParserTestCase do
  use Support.ParserTestCase

  describe "regex_parser from a string" do
    test "fails on empty" do
      assert_failure(
        subject(),
        "",
        "~r{\\A[a-e0-2]}u does not match at 1,1 in rgx_match_parser([a-e0-2]) (in rgx_match_parser([a-e0-2]))"
      )
    end

    test "fails on ws" do
      assert_failure(
        subject(),
        " ",
        "~r{\\A[a-e0-2]}u does not match at 1,1 in rgx_match_parser([a-e0-2]) (in rgx_match_parser([a-e0-2]))"
      )
    end

    test "fails on arbitrary char" do
      assert_failure(
        subject(),
        "%",
        "~r{\\A[a-e0-2]}u does not match at 1,1 in rgx_match_parser([a-e0-2]) (in rgx_match_parser([a-e0-2]))"
      )
    end

    test "fails on non matching letter" do
      assert_failure(
        subject(),
        "f",
        "~r{\\A[a-e0-2]}u does not match at 1,1 in rgx_match_parser([a-e0-2]) (in rgx_match_parser([a-e0-2]))"
      )
    end

    test "fails on non matching digit" do
      assert_failure(
        subject(),
        "3",
        "~r{\\A[a-e0-2]}u does not match at 1,1 in rgx_match_parser([a-e0-2]) (in rgx_match_parser([a-e0-2]))"
      )
    end

    test "succeds on matching letter" do
      assert_success(subject(), "a", "a", make_input("", 2))
    end

    test "succeds on matching digit" do
      assert_success(subject(), "2ello", "2", make_input("ello", 2))
    end

    defp subject, do: rgx_match_parser("[a-e0-2]")
  end

  describe "regex_parser from a regex" do
    test "fails on a letter" do
      assert_failure(
        subject1(),
        "Hello",
        "~r{\\A[[:digit:]]}u does not match at 1,1 in rgx_match_parser([[:digit:]]) (in rgx_match_parser([[:digit:]]))"
      )
    end

    test "succeds on a digit" do
      assert_success(subject1(), "9ello", "9", make_input("ello", 2))
    end

    defp subject1, do: rgx_match_parser("[[:digit:]]")
  end

  describe "rgx_capture_parser" do
    test "does not match" do
      assert_failure(
        subject2(),
        "a",
        "~r{\\A\\s+(.)}u does not match at 1,1 in rgx_capture_parser(\\s+(.)) (in rgx_capture_parser(\\s+(.)))"
      )
    end

    defp subject2, do: rgx_capture_parser("\\s+(.)")
  end
end

# SPDX-License-Identifier: Apache-2.0
