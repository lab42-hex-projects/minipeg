defmodule Test.Minipeg.Parsers.UptoParserTestCase do
  use Support.ParserTestCase

  describe "empty" do
    test "ok" do
      assert_success(
        subject(),
        "",
        "",
        ""
      )
    end

    test "not ok, min_count > 0" do
      assert_failure(
        subject(1),
        "",
        "Missing 1 parses in many (in my parser)"
      )
    end
  end

  describe "parsing to the end" do
    test "long enough" do
      assert_success(
        subject(2),
        "ab",
        "ab",
        make_input("", 3)
      )
    end

    test "too short" do
      assert_failure(
        subject(3),
        "ab",
        "Missing 1 parses in many (in my parser)"
      )
    end
  end

  describe "parsing to a stop char" do
    test "first stop char" do
      assert_success(
        subject(),
        "ab<",
        "ab",
        make_input("", 4)
      )
    end

    test "second stop char" do
      assert_success(
        subject(),
        "!",
        "",
        make_input("", 2)
      )
    end

    test "not long enough, again" do
      assert_failure(
        subject(2),
        "a!",
        "Missing 1 parses in many (in my parser)"
      )
    end
  end

  defp subject(min_count \\ 0), do: upto_parser("<!", min_count, "my parser")
end

# SPDX-License-Identifier: Apache-2.0
