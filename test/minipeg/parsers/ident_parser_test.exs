defmodule Test.Minipeg.Parsers.IdentParserTestCase do
  use Support.ParserTestCase

  test "lispy style ident parser" do
    assert_parse_string_ok(
      ident_parser("Lisp Style", additional_chars: "-"),
      "hello-42",
      "hello-42"
    )
  end

  test "no underscores allowed" do
    assert_parse_string_ok(
      ident_parser("No undercores", additional_chars: nil),
      "hello_42",
      "hello"
    )
  end
end

# SPDX-License-Identifier: Apache-2.0
