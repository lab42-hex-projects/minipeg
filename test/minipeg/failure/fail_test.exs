defmodule Test.Minipeg.Failure.FailTest do
  use ExUnit.Case

  alias Minipeg.{Cache, Failure, Input}
  import Minipeg.Failure, only: [fail: 3]

  describe "Failure factory" do
    test "with 3 values" do
      cache = %Cache{}
      input = Input.new("some input")

      failure = fail("an error", input, cache)
      assert failure == %Failure{reason: "an error", cache: cache, input: input}
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
