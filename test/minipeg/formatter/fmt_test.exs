defmodule Test.Minipeg.Formatter.SimpleTest do
  use Support.ParserTestCase
  import Formatter

  @failure %Failure{
    input: %Input{col: 23, input: "some text", lnb: 73},
    parsed_by: "a parser",
    reason: "bad syntax"
  }

  @input %Input{
    col: 23,
    input: "some text",
    lnb: 73
  }

  @success %Success{
    ast: "some",
    parsed_by: "a parser",
    parsed_at: {23, 73},
    rest: %Input{col: 27, input: " text", lnb: 73}
  }

  describe "for %Failure" do
    test "short" do
      assert fmt(:short, @failure) ==
        "Failure: bad syntax"
    end

    test "short with label" do
      assert fmt(:short, @failure, :failure) ==
        "Failure(failure): bad syntax"
    end
  end

  describe "for %Input" do
    test "short" do
      assert fmt(:short, @input) ==
        "Input: 23,73>some text"
    end
    test "short with label" do
      assert fmt(:short, @input, "an input") ==
        "Input(an input): 23,73>some text"
    end
  end

  describe "for %Success" do
    test "short" do
      assert fmt(:short, @success) ==
        ~s{Success: "some"}
    end
    test "short with label" do
      assert fmt(:short, @success, 42) ==
        ~s{Success(42): "some"}
    end
  end

  describe "format" do
    test "short success" do
      assert format(:short).(@input, @success) ==
        [
          "Input: 23,73>some text",
          ~s{Success: "some"}
        ]
        |> Enum.join("\n")
    end

    test "short failure with label" do
      assert format(:short, "Error").(@input, @failure)  ==
        [
          "Error",
          "Input: 23,73>some text",
          "Failure: bad syntax"
        ]
        |> Enum.join("\n")
      
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
