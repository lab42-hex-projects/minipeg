defmodule Test.Minipeg.NonReg.I001PositionAdvanceInWsTest do
  @moduledoc ~S"""
  Related to [Issue #1](https://gitlab.com/lab42-hex-projects/minipeg/-/issues/1)
  """

  use Support.ParserTestCase

  describe "missing advancement in rest field of %Success{}" do
    test "... in first line" do
      parser = ignore_ws(end_parser())
      %Success{} = parse(parser, " \t")
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
