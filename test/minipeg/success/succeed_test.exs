defmodule Test.Minipeg.Success.SucceedTest do
  use ExUnit.Case

  alias Minipeg.{Cache, Input, Success}
  import Minipeg.Success, only: [succeed: 4]

  describe "Success factory" do
    test "with 3 values" do
      cache = %Cache{}
      input = Input.new("some input")

      success = succeed("an ast", input, cache, {2, 7})
      assert success == %Success{ast: "an ast", cache: cache, parsed_at: {2, 7}, rest: input}
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
