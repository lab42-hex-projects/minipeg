defmodule Test.Minipeg.Mappers.IgnoreFirstTest do
  use Support.ParserTestCase

  describe "ignore_first" do
    test "in a simple list" do
      assert_success(
        sequence([
          char_parser("a"),
          char_parser("b")
        ])
        |> map(&ignore_first/1),
        "abc",
        ["b"],
        make_input("c", 3)
      )
    end

    test "does not flatten" do
      assert_success(
        sequence([
          char_parser("a"),
          sequence([
            char_parser("b"),
            char_parser("c")
          ])
        ])
        |> map(&ignore_first/1),
        "abc",
        [["b", "c"]],
        make_input("", 4)
      )
    end
  end

  describe "ignore_first_and_flatten" do
    test "in a simple list, same as above" do
      assert_success(
        sequence([
          char_parser("a"),
          char_parser("b")
        ])
        |> map(&ignore_first_and_flatten/1),
        "abc",
        ["b"],
        make_input("c", 3)
      )
    end

    test "flattens" do
      assert_success(
        sequence([
          char_parser("a"),
          sequence([
            char_parser("b"),
            char_parser("c")
          ])
        ])
        |> map(&ignore_first_and_flatten/1),
        "abc",
        ["b", "c"],
        make_input("", 4)
      )
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
