defmodule Test.Minipeg.SatisfiersTest do
  use ExUnit.Case

  describe "len_in_range" do
    test "min or max needed" do
      assert_raise(ArgumentError, fn ->
        Minipeg.Satisfiers.len_in_range(nil)
      end)
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
