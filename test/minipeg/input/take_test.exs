defmodule Test.Minipeg.Input.TakeTest do
  use ExUnit.Case

  alias Minipeg.Input

  import Input, only: [take: 2]

  @inp Input.new("a\nbc\n")
  describe "taking available" do
    test "just one" do
      assert take(@inp, 1) == {"a", Input.drop(@inp, 1)}
    end

    test "by string" do
      assert take(@inp, "a.bc") == {"a\nbc", Input.drop(@inp, 4)}
    end
  end

  describe "taking all" do
    assert take(@inp, 1000) == {"a\nbc\n", Input.new("", 1, 3)}
  end
end

# SPDX-License-Identifier: Apache-2.0
