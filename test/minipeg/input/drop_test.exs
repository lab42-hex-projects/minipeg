defmodule Test.Minipeg.Input.DropTest do
  use ExUnit.Case

  alias Minipeg.Input
  import Input, only: [drop: 1, drop: 2]

  describe "drop with available" do
    test "dropping chars by count" do
      inp = Input.new("alpha")
      assert drop(inp, 2) == Input.new("pha", 3)
    end

    test "dropping by default" do
      inp = Input.new("alpha")
      assert drop(inp) == Input.new("lpha", 2)
    end

    test "dropping by a string" do
      inp = Input.new("alpha")
      assert drop(inp, "how") == Input.new("ha", 4)
    end
  end

  describe "dropping more than available" do
    test "empty" do
      empty = Input.new("")
      assert drop(empty) == empty
    end

    test "too long" do
      inp = Input.new("alpha")
      assert drop(inp, "Hello!") == Input.new("", 6)
    end
  end

  describe "counting lines" do
    test "one line" do
      inp = Input.new("a\nb\nc")
      assert drop(inp, 2) == Input.new("b\nc", 1, 2)
    end

    test "two lines" do
      inp = Input.new("a\nb\ncd")
      assert drop(inp, "a.b.c") == Input.new("d", 2, 3)
    end
  end

  describe "dropping nothing" do
    test "is idempotent" do
      inp = Input.new("a\nb\nc")
      assert drop(inp, 0) == inp
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
