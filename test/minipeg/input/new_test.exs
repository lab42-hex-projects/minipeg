defmodule Test.Minipeg.Input.NewTest do
  use ExUnit.Case

  alias Minipeg.Input

  describe "new" do
    test "creation with defaults" do
      assert Input.new("C'est ça") == %Input{input: "C'est ça"}
    end

    test "creation with col" do
      assert Input.new("Hello", 2) == %Input{input: "Hello", col: 2}
    end

    test "empty works too" do
      assert Input.new("", 2, 3) == %Input{input: "", col: 2, lnb: 3}
    end

    test "it can take an Input struct" do
      inp = Input.new("a", 10, 20)
      assert Input.new(inp, 11, 21) == %Input{input: "a", col: 11, lnb: 21}
    end

    test "graphemes can also be provided" do
      assert Input.new("abc") == %Input{input: "abc"}
    end
  end

  describe "position" do
    test "get a tuple from Input" do
      inp = Input.new("a", 10, 20)
      assert Input.position(inp) == {10, 20}
    end
  end

  describe "report_position" do
    test "w/o a name" do
      inp = Input.new("a", 10, 20)
      assert Input.report_position(inp) == "20,10"
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
