defmodule Test.Minipeg.Parser.NewTest do
  use ExUnit.Case

  alias Minipeg.{Failure, Parser}

  describe "Parser" do
    test "creation" do
      parser = Parser.new("anon", &parser_function/2)
      assert parser == %Parser{name: "anon", parser_function: &parser_function/2}
    end

    defp parser_function(input, cache), do: Failure.fail("bad input", input, cache)
  end
end

# SPDX-License-Identifier: Apache-2.0
