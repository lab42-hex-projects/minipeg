defmodule Test.Minipeg.Parser.ParseTest do
  use ExUnit.Case

  alias Minipeg.{Cache, Failure, Input, Parser, Parsers, Success}

  import Parser, only: [new: 2, parse: 3, parse_string: 2]

  describe "Parsing" do
    test "failure" do
      %Failure{reason: reason, input: input1, cache: %Cache{}} =
        parse(failure_parser(), input(), %Cache{})

      assert input1 == input()
      assert reason == "failed failure parser"
    end

    test "success" do
      %Success{ast: ast, rest: rest, cache: %Cache{}} = parse(success_parser(), input(), %Cache{})
      assert rest == %Input{input: "ome data", col: 2, lnb: 1}
      assert ast == "succeeded success parser"
    end

    defp failure_parser do
      new(
        "failure parser",
        fn input, cache, name -> Failure.fail("failed #{name}", input, cache) end
      )
    end

    defp input, do: Input.new("some data")

    defp success_parser do
      new(
        "success parser",
        fn input, cache, name ->
          Success.succeed("succeeded #{name}", Input.drop(input), cache, {2, 5})
        end
      )
    end
  end

  describe "Parsing a string" do
    test "failure" do
      {:error, "failed failure parser in <binary>:1,1"} = parse_string(failure_parser(), "whatever")
    end

    test "success" do
      {:ok, "a"} = parse_string(Parsers.char_parser("a"), "a")
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
