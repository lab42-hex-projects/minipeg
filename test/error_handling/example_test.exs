defmodule Test.ErrorHandling.ExampleTest do
  use Support.ParserTestCase

  describe "error handling example with satisfy and nameing" do

    test "bad message" do
      %Failure{} = f = parse(parser(), "(ab]")
      assert f.parsed_by == "short expression"
    end
    defp parser() do
      sequence([
        char_parser("(["),
        many(char_parser("ab")),
        char_parser(")]"),
        ], "short expression")
        |> satisfy(&matcher/1)
    end
  end

  defp matcher(ast)
  defp matcher(["(", inner, ")"]), do: {:ok, inner}
  defp matcher(["[", inner, "]"]), do: {:ok, inner}
  defp matcher([lhs, _, rhs]), do: {:error, "paren missmatch: #{lhs}, #{rhs}"}
end
# SPDX-License-Identifier: Apache-2.0
