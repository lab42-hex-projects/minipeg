defmodule Test.MinipegTest do
  use ExUnit.Case

  import Minipeg.Parser, only: [parse_string: 2]
  import Minipeg.{Combinators, Parsers}

  alias Minipeg.{Cache, Input, Parser, Success}

  doctest Minipeg, import: true

  def parse(parser, input) do
    Minipeg.Parser.parse(parser, Minipeg.Input.new(input), Minipeg.Cache.empty())
  end
end

# SPDX-License-Identifier: Apache-2.0
