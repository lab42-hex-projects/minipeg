defmodule Minipeg.Mappers do
  use Minipeg.Types

  @moduledoc ~S"""
  Define some useful functions for `Minipeg.Combinators.map`
  """

  @doc ~S"""
  A mapper for parsers that return not empty lists, it removes the first element from the mapper

  Ex.:
    
          sequence([
            char_parser("%"),
            some_parser()        # --> some_ast
          ]) |> map(&ignore_first/1) # --> [some_ast]

          sequence([
            char_parser("%"),
            some_parser()        # --> some_ast
            other_parser()        # --> other_ast
          ]) |> map(&ignore_first/1) # --> [some_ast, other_ast]
  """
  @spec ignore_first(ast_list_t()) :: ast_t()
  def ignore_first(ast_list)
  def ignore_first([_ | rest]), do: rest

  @doc ~S"""
  A mapper for parsers that return not empty lists, it removes the first element from the mapper
  and then flattens the rest

  Ex.:
          sequence([
            char_parser("%"),
            sequence([
            some_parser()        # --> some_ast
            other_parser()        # --> other_ast
            ])
          ]) |> map(&ignore_first/1) # --> [some_ast, other_ast] and not [[..., ...]]
  """
  @spec ignore_first_and_flatten(ast_list_t()) :: ast_list_t()
  def ignore_first_and_flatten(ast_list)
  def ignore_first_and_flatten([_ | rest]), do: rest |> List.flatten()
end

# SPDX-License-Identifier: Apache-2.0
