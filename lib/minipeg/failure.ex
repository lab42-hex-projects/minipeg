defmodule Minipeg.Failure do
  alias Minipeg.{Cache, Input}
  use Minipeg.Types

  @moduledoc ~S"""
  A failure result
  """

  defstruct cache: %Cache{}, input: %Input{}, parsed_by: "", reason: ""

  @type t :: %__MODULE__{cache: Cache.t(), input: Input.t(), parsed_by: binary(), reason: binary()}

  @spec fail(binary(), Input.t(), Cache.t(), binary()) :: t
  def fail(reason, input, cache, parsed_by \\ "") do
    %__MODULE__{cache: cache, input: input, parsed_by: parsed_by, reason: reason}
  end

  @spec format(t()) :: binary()
  def format(%__MODULE__{input: input}=failure) do
    source_name = Map.get(input, :context, %{})
             |> Map.get(:source_name, "<binary>")
    "#{error_message(failure)} in #{source_name}:#{Input.report_position(input)}"
  end

  @spec error_message(t()) :: binary()
  def error_message(f)
  def error_message(%__MODULE__{parsed_by: ""}=f) do
    f.reason
  end
  def error_message(%__MODULE__{}=f) do
    "#{f.reason} (in #{f.parsed_by})"
  end

  @spec reset_error(t(), binary?(), binary?()) :: t()
  def reset_error(f, reason, parser_name \\ nil) do
    %{f|parsed_by: parser_name || f.parsed_by, reason: reason || f.reason}
  end
end
# SPDX-License-Identifier: Apache-2.0
