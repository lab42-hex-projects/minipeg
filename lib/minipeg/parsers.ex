defmodule Minipeg.Parsers do
  use Minipeg.Types

  alias Minipeg.{Cache, Combinators, Input, Failure, Parser, Satisfiers, Success}

  import Combinators
  import Failure, only: [fail: 4]
  import Parser, only: [new: 2]
  import Success, only: [succeed: 5]

  @moduledoc ~S"""
  A collection of basic parsers
  """

  @typep char_set_spec_t :: maybe(binary() | list())
  @type char_class_t ::
          :alnum
          | :alpha
          | :blank
          | :cntrl
          | :digit
          | :graph
          | :lower
          | :print
          | :punct
          | :space
          | :upper
          | :word
          | :xdigit

  @doc ~S"""
  Parses any character in the specified set, defaulting to all characters

  Succeeds if next char in input is in the specified set
  Fails if input is empty or next char is not in the specified set

  If the specified charset is an atom it is interpreted as a POSIX character class
  described in the docs of the `Regex` module, here are the currently supported values:

  ```
    :alnum | :alpha | :blank | :cntrl | :digit | :graph | :lower | :print | :punct | :space | :upper | :word | :xdigit
  ```
  """
  @spec char_parser(maybe(atom() | binary() | list()), binary?) :: Parser.t()
  def char_parser(specified_set \\ nil, name \\ nil)

  def char_parser(nil, name) do
    name1 = name || "char_parser()"

    new(
      name1,
      &_any_char_parser/3
    )
  end

  def char_parser(char_class, name) when is_atom(char_class) do
    name1 = name || "char_parser(:#{char_class})"
    rgx_match_parser("[[:#{char_class}:]]", name1)
  end

  def char_parser(specified_set, name) do
    name1 = name || "char_parser(#{inspect(specified_set)})"

    new(
      name1,
      &_any_char_parser/3
    )
    |> satisfy(&_satisfy_char_membership(&1, specified_set, name1))
  end

  @spec delimited_string_parser(binary(), binary(), boolean(), binary?()) :: Parser.t()
  def delimited_string_parser(
        delimiter,
        escape_char \\ "\\",
        allow_double_escapes \\ true,
        name \\ nil
      ) do
    # {delimiter}")"
    name1 = name || "delimited_string_parser("

    sequence(
      [
        char_parser(delimiter),
        many(maybe_escaped_char_parser(delimiter, escape_char, allow_double_escapes)),
        char_parser(delimiter)
      ],
      name1
    )
    |> map(fn [_, content, _] -> content end, name1)
  end

  @doc ~S"""
  Parser that only succeeds on empty input / end of input
  """
  @spec end_parser(binary?) :: Parser.t()
  def end_parser(name \\ nil) do
    name1 = name || "end_parser"

    new(
      name1,
      &_end_parser_fun(name1, &1, &2, &3)
    )
  end

  @doc ~S"""
  parses an escape char and then any char
  """
  @spec escaped_char_parser(binary?(), binary?(), char_set_spec_t()) :: Parser.t()
  def escaped_char_parser(escape_char \\ "\\", name \\ nil, allowed_set \\ nil) do
    name1 = name || "escaped_char_parser(#{escape_char})"

    rhs_parser =
      if allowed_set, do: char_parser(allowed_set), else: char_parser()

    sequence(
      [
        char_parser(escape_char),
        rhs_parser
      ],
      name1
    )
    |> set_error(nil, "escaped_char_parser")
    |> map(fn [_, escaped_char] -> escaped_char end)
  end

  @doc ~S"""
  This parser always fails
  """
  @spec failure_parser(binary?, binary?) :: Parser.t()
  def failure_parser(reason \\ nil, name \\ nil) do
    name1 = name || "failure_parser"
    reason1 = reason || "this parser always fails"

    new(
      name1,
      fn input, cache, _name ->
        fail(reason1, input, cache, name1)
      end
    )
  end

  @spec ident_parser(binary?(), Keyword.t()) :: Parser.t()
  def ident_parser(name \\ nil, opts \\ []) do
    additional_chars = Keyword.get(opts, :additional_chars, "_")
    first_char_parser = Keyword.get(opts, :first_char_parser, char_parser(:alpha))
    rest_char_parser = Keyword.get(opts, :rest_char_parser, char_parser(:alnum))
    max_len = Keyword.get(opts, :max_len)
    min_len = Keyword.get(opts, :min_len)
    name1 = name || "ident_parser(#{additional_chars})"
    _ident_parser(name1, first_char_parser, rest_char_parser, additional_chars, max_len, min_len)
  end

  @spec int_parser(binary?) :: Parser.t()
  def int_parser(name \\ nil) do
    name1 = name || "int_parser"

    sequence(
      [
        maybe(char_parser("-+")),
        many(char_parser("0123456789"), "int_parser", 1)
      ],
      name1
    )
    |> map(&_int_map_fun/1)
  end

  @doc ~S"""
  Succeeds if any of the given keywords parse
  """
  @spec keywords_parser(list(binary()), binary?) :: Parser.t()
  def keywords_parser(keywords, name \\ nil) do
    name1 = name || "keywords_parser(#{inspect(keywords)})"

    keywords
    |> Enum.map(&literal_parser/1)
    |> select(name1)
  end

  @doc ~S"""
  Parses only an exact occurrance of the `literal` string
  """
  @spec literal_parser(binary(), binary?) :: Parser.t()
  def literal_parser(literal, name \\ nil) do
    name1 = name || "literal_parser(#{inspect(literal)})"

    literal
    |> String.graphemes()
    |> Enum.map(&char_parser/1)
    |> sequence(name1)
    |> map(&Enum.join/1)
  end

  @doc ~S"""
  Parses any character but delimiter, unless it is escpaed by `escape_char` or 
  doubled in case `allow_doubles` is true.
  """
  @spec maybe_escaped_char_parser(binary(), binary(), boolean(), binary?()) :: Parser.t()
  def maybe_escaped_char_parser(
        delimiter,
        escape_char \\ "\\",
        allow_doubles \\ true,
        name \\ nil
      ) do
    name1 = name || "maybe_escaped_char_parser(#{delimiter}, #{escape_char}, #{allow_doubles}"

    if allow_doubles do
      select(
        [
          escaped_char_parser(escape_char),
          not_char_parser(delimiter),
          literal_parser(delimiter <> delimiter) |> map(fn _ -> delimiter end)
        ],
        name1
      )
    else
      select(
        [
          escaped_char_parser(escape_char),
          not_char_parser(delimiter)
        ],
        name1
      )
    end
  end

  @doc ~S"""
  Parses any char with the exception of chars in the `forbidden` set
  """
  @spec not_char_parser(char_set_t(), binary?) :: Parser.t()
  def not_char_parser(forbidden, name \\ nil)

  def not_char_parser(forbidden, name) when is_binary(forbidden) do
    forbidden |> String.graphemes() |> not_char_parser(name)
  end

  def not_char_parser(forbidden, name) when is_list(forbidden) do
    name1 = name || "not_char_parser(#{Enum.join(forbidden)})"

    char_parser(nil, name1)
    |> satisfy(fn char ->
      if Enum.member?(forbidden, char) do
        {:error, "#{char} is in forbidden set #{inspect(Enum.join(forbidden))} in #{name1}"}
      else
        {:ok, char}
      end
    end)
  end

  @doc ~S"""
  parses only if parser parses a string with a prefix that is discarded
  """
  @spec prefixed_parser(char_set_t(), Parser.t(), binary?()) :: Parser.t()
  def prefixed_parser(prefix, parser, name \\ nil)

  def prefixed_parser(prefix, parser, name) when is_binary(prefix) do
    prefix |> String.graphemes() |> prefixed_parser(parser, name) |> set_error("prefix #{inspect prefix} not found")
  end

  def prefixed_parser(prefixes, parser, name) do
    name1 = name || "prefixed_by(#{inspect(prefixes)}, #{parser.name})"

    sequence(
      [
        keywords_parser(prefixes),
        parser
      ],
      name1
    )
    |> set_error("none of the strings #{inspect(prefixes)} found at current position", name)
    |> map(fn [_, ast] -> ast end)
  end

  @spec rgx_capture_parser(binary(), binary?(), list(atom())) :: Parser.t()
  def rgx_capture_parser(rgx_string, name \\ nil, options \\ [])

  def rgx_capture_parser(rgx_string, name, options) when is_binary(rgx_string) do
    name1 = name || "rgx_capture_parser(#{rgx_string})"
    rgx_rep = "~r{\\A" <> rgx_string <> "}u"
    rgx = Regex.compile!("\\A" <> rgx_string, [:unicode | options])

    new(
      name1,
      &_rgx_capture_parser(rgx, &1, &2, &3, rgx_rep)
    )
  end

  @spec rgx_match_parser(binary(), binary?(), list(atom())) :: Parser.t()
  def rgx_match_parser(rgx_string, name \\ nil, options \\ [])

  def rgx_match_parser(rgx_string, name, options) when is_binary(rgx_string) do
    name1 = name || "rgx_match_parser(#{rgx_string})"
    rgx_rep = "~r{\\A" <> rgx_string <> "}u"
    rgx = Regex.compile!("\\A" <> rgx_string, [:unicode | options])

    new(
      name1,
      &_rgx_match_parser(rgx, &1, &2, &3, rgx_rep)
    )
  end

  @spec rgx_parser(binary(), binary?(), list(atom())) :: Parser.t()
  def rgx_parser(rgx_string, name \\ nil, options \\ [])

  def rgx_parser(rgx_string, name, options) when is_binary(rgx_string) do
    name1 = name || "rgx_parser(#{rgx_string})"
    rgx_rep = "~r{\\A" <> rgx_string <> "}u"
    rgx = Regex.compile!("\\A" <> rgx_string, [:unicode | options])

    new(
      name1,
      &_rgx_parser(rgx, &1, &2, &3, rgx_rep)
    )
  end

  @doc ~S"""
   A _classic_ string parser which parses input like

   - `""`
   - `'"a\\'b"c'`
   - `"ab""c"`

   but not

   - `'ab`
   - `"abc'`
   - `"ad"e` (e not parsed)
  """
  @spec string_parser(char_set_t(), binary?(), boolean(), binary?()) :: Parser.t()
  def string_parser(
        paren_chars \\ ["\"", "'"],
        escape_char \\ "\\",
        allow_double_escapes \\ true,
        name \\ nil
      ) do
    name1 = name || "string_parser"

    _string_parser(paren_chars, escape_char, allow_double_escapes, name1)
    |> map(&IO.chardata_to_string/1)
  end

  @doc ~S"""
  This parser always succeeds.
  **N.B.** It does not advance the input
  """
  @spec success_parser(ast_t(), binary?) :: Parser.t()
  def success_parser(return_ast \\ nil, name \\ nil) do
    name1 = name || "success_parser"

    new(
      name1,
      fn input, cache, _ ->
        succeed(return_ast, input, cache, Input.position(input), name1)
      end
    )
  end

  @spec unsigned_int_parser(binary?) :: Parser.t()
  def unsigned_int_parser(name \\ nil) do
    name1 = name || "unsigned_int_parser"

    many(char_parser("0123456789"), name1, 1)
    |> map(&_int_map_fun/1)
  end

  @doc ~S"""
  parses a string up to the first char in `charset` fails iff less then `min_count` chars are parsed.
  If a character from `charset` is encountered it is removed from the input stream, but is not returned in the ast.
  """
  @spec upto_parser(char_set_t(), non_neg_integer(), binary?()) :: Parser.t()
  def upto_parser(charset, min_count \\ 0, name \\ nil) do
    name1 = name || "upto_parser(#{inspect(charset)}, #{min_count})"

    sequence([
      many(not_char_parser(charset), name1, min_count),
      maybe(char_parser(charset))
    ])
    |> map(fn [inner, _endchar] -> Enum.join(inner) end)
  end

  @doc ~S"""
  Parses a sequence of at least `min_count` whitespaces.
  `min_count` defaults to 0
  """
  @spec ws_parser(boolean(), non_neg_integer(), binary?) :: Parser.t()
  def ws_parser(allow_newline \\ false, min_count \\ 0, name \\ nil) do
    name1 = name || "ws_parser(#{allow_newline}, #{min_count})"
    charset = if allow_newline, do: "\n\t ", else: "\t "
    many(char_parser(charset), name1, min_count)
  end

  @spec _any_char_parser(Input.t(), Cache.t(), binary()) :: result_t()
  defp _any_char_parser(input, cache, name) do
    case input.input do
      "" ->
        fail("encountered end of input", input, cache, name)

      _ ->
        {char, input1} = Input.take(input, 1)
        succeed(char, input1, cache, Input.position(input), name)
    end
  end

  @spec _end_parser_fun(binary(), Input.t(), Cache.t(), binary()) :: result_t
  defp _end_parser_fun(name, input, cache, _name) do
    case input.input do
      "" ->
        succeed(nil, input, cache, Input.position(input), name)

      _ ->
        fail(
          "not at end of input @ #{inspect(Input.position(input))} in #{name}",
          input,
          cache,
          name
        )
    end
  end

  @spec _ident_parser(
          binary(),
          Parser.t(),
          Parser.t(),
          char_set_spec_t(),
          maybe(non_neg_integer()),
          maybe(non_neg_integer())
        ) :: Parser.t()
  defp _ident_parser(
         name,
         first_char_parser,
         rest_char_parser,
         additional_chars,
         max_len,
         min_len
       )

  defp _ident_parser(name, first_char_parser, rest_char_parser, nil, nil, nil) do
    sequence([
      first_char_parser,
      many(
        select(
          [
            rest_char_parser
          ],
          name
        )
      )
    ])
    |> map(&IO.chardata_to_string/1)
  end

  defp _ident_parser(name, first_char_parser, rest_char_parser, additional_chars, nil, nil) do
    sequence([
      first_char_parser,
      many(
        select(
          [
            rest_char_parser,
            char_parser(additional_chars)
          ],
          name
        )
      )
    ])
    |> map(&IO.chardata_to_string/1)
  end

  defp _ident_parser(name, first_char_parser, rest_char_parser, nil, max_len, min_len) do
    sequence([
      first_char_parser,
      many(
        select(
          [
            rest_char_parser
          ],
          name
        )
      )
    ])
    |> map(&IO.chardata_to_string/1, name)
    |> satisfy(&Satisfiers.len_in_range(&1, max_len, min_len), name)
  end

  defp _ident_parser(
         name,
         first_char_parser,
         rest_char_parser,
         additional_chars,
         max_len,
         min_len
       ) do
    sequence([
      first_char_parser,
      many(
        select(
          [
            rest_char_parser,
            char_parser(additional_chars)
          ],
          name
        )
      )
    ])
    |> map(&IO.chardata_to_string/1)
    |> satisfy(&Satisfiers.len_in_range(&1, max_len, min_len), name)
  end

  @spec _int_map_fun(ast_t()) :: ast_t()
  defp _int_map_fun(ast)
  defp _int_map_fun([nil | rest]), do: _int_map_fun(rest)

  defp _int_map_fun(ast) do
    ast
    |> IO.chardata_to_string()
    |> String.to_integer()
  end

  @spec _rgx_capture_parser(Regex.t(), Input.t(), Cache.t(), binary(), binary()) :: result_t()
  defp _rgx_capture_parser(rgx, input, cache, name, rgx_rep) do
    case Regex.run(rgx, input.input) do
      nil ->
        fail("#{rgx_rep} does not match at #{Input.report_position(input, name)}", input, cache, name)

      [match | captures] ->
        input1 = Input.drop(input, match)
        succeed(List.first(captures), input1, cache, Input.position(input), name)
    end
  end

  @spec _rgx_match_parser(Regex.t(), Input.t(), Cache.t(), binary(), binary()) :: result_t()
  defp _rgx_match_parser(rgx, input, cache, name, rgx_rep) do
    case Regex.run(rgx, input.input) do
      nil ->
        fail("#{rgx_rep} does not match at #{Input.report_position(input, name)}", input, cache, name)

      [match | _] ->
        input1 = Input.drop(input, match)
        succeed(match, input1, cache, Input.position(input), name)
    end
  end

  @spec _rgx_parser(Regex.t(), Input.t(), Cache.t(), binary(), binary()) :: result_t()
  defp _rgx_parser(rgx, input, cache, name, rgx_rep) do
    case Regex.run(rgx, input.input) do
      nil ->
        fail("#{rgx_rep} does not match at #{Input.report_position(input, name)}", input, cache, name)

      [match | _captures] = matches ->
        input1 = Input.drop(input, match)
        succeed(matches, input1, cache, Input.position(input), name)
    end
  end

  @spec _satisfy_char_membership(binary(), char_set_spec_t(), binary()) :: satisfier_result_t
  defp _satisfy_char_membership(char, char_spec, name)

  defp _satisfy_char_membership(char, char_spec, name) when is_binary(char_spec) do
    _satisfy_char_membership(char, char_spec |> String.graphemes(), name)
  end

  defp _satisfy_char_membership(char, char_spec, name) when is_list(char_spec) do
    if Enum.member?(char_spec, char) do
      {:ok, char}
    else
      {:error, "#{char} not member of #{char_spec |> Enum.join() |> inspect()} in #{name}"}
    end
  end

  @spec _string_parser(char_set_t(), binary?(), boolean(), binary()) :: Parser.t()
  defp _string_parser(paren_chars, escape_char, allow_double_escapes, name)

  defp _string_parser(paren_chars, escape_char, allow_double_escapes, name)
       when is_binary(paren_chars) do
    paren_chars
    |> String.graphemes()
    |> _string_parser(escape_char, allow_double_escapes, name)
  end

  defp _string_parser(paren_chars, escape_char, allow_double_escapes, name)
       when is_list(paren_chars) do
    name1 = name || "string_parser(#{inspect(paren_chars |> Enum.join())})"

    select(
      paren_chars
      |> Enum.map(&delimited_string_parser(&1, escape_char, allow_double_escapes, name1)),
      name1
    )
  end
end

# SPDX-License-Identifier: Apache-2.0
