defmodule Minipeg.Ignore do
  use Minipeg.Types

  alias Minipeg.{Cache, Input}

  @moduledoc ~S"""
  An ignored success result
  """
  defstruct cache: %Cache{}, parsed_at: {1, 1}, rest: %Input{}

  @type t :: %__MODULE__{cache: Cache.t(), rest: Input.t()}

  @spec ignore(Input.t(), Cache.t()) :: t
  def ignore(rest, cache) do
    %__MODULE__{cache: cache, rest: rest}
  end
end

# SPDX-License-Identifier: Apache-2.0
