defmodule Minipeg.Success do
  use Minipeg.Types

  alias Minipeg.{Cache, Input}

  @moduledoc ~S"""
  A success result
  """
  defstruct ast: nil, cache: %Cache{}, parsed_at: {1, 1}, parsed_by: "", rest: %Input{}

  @type t :: %__MODULE__{ast: any(), cache: Cache.t(), parsed_at: position_t(), parsed_by: binary(), rest: Input.t()}

  @spec succeed(ast_t(), Input.t(), Cache.t(), position_t(), binary()) :: t
  def succeed(ast, rest, cache, position, parsed_by \\ "") do
    %__MODULE__{ast: ast, cache: cache, parsed_at: position, parsed_by: parsed_by, rest: rest}
  end
end
# SPDX-License-Identifier: Apache-2.0
