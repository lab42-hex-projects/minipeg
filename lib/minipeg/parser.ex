defmodule Minipeg.Parser do
  use Minipeg.Types

  alias Minipeg.{Cache, Failure, Input, Success}

  @moduledoc ~S"""
  A struct containung a parser function and a name
  """

  defstruct name: "",
            parser_function: nil

  @type t :: %__MODULE__{parser_function: parser_function_t(), name: binary()}
  @type result_tuple_t(ast_t) :: {:ok, ast_t} | {:error, binary()}

  @spec new(binary(), parser_function_t()) :: t
  def new(name, parser_function), do: %__MODULE__{name: name, parser_function: parser_function}

  @spec parse(t(), Input.t(), Cache.t()) :: result_t
  def parse(%__MODULE__{parser_function: fun, name: name}, input, cache) do
    fun.(input, cache, name)
  end

  @spec parse_string(t(), binary()) :: result_tuple_t(any())
  def parse_string(%__MODULE__{parser_function: fun, name: name}, input_string) do
    case fun.(Input.new(input_string), Cache.empty(), name) do
      %Failure{} = f -> {:error, Failure.format(f)}
      %Success{ast: ast} -> {:ok, ast}
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
