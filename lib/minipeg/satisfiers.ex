defmodule Minipeg.Satisfiers do
  use Minipeg.Types
  @moduledoc false

  @spec len_in_range(binary(), maybe(non_neg_integer()), maybe(non_neg_integer())) ::
          satisfier_result_t()
  def len_in_range(ast, max_len \\ nil, min_len \\ nil)

  def len_in_range(_ast, nil, nil) do
    raise ArgumentError, "cannot satisfy strings length without constraint"
  end

  def len_in_range(ast, max_len, min_len) do
    cond do
      max_len && String.length(ast) > max_len ->
        {:error,
         "string #{ast |> inspect} length #{String.length(ast)} exceeds allowed #{max_len}"}

      min_len && String.length(ast) < min_len ->
        {:error,
         "string #{ast |> inspect} length #{String.length(ast)} under required minimum #{min_len}"}

      true ->
        {:ok, ast}
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
